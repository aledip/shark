import json
import socket
from time import sleep

import requests


class Node:

    def __init__(self,name='',ip= socket.gethostbyname(socket.gethostname()),port = 8080,is_master = False):
        self.name = name
        self.links = []
        self.ip = "http://"+ ip
        self.master = None
        self.is_master = is_master
        self.is_connected = False
        self.port = str(port)

        # while True:
        #     if not self.is_connected:
        #         try:
        #             if not self.master is None:
        #                 self.register()
        #             else:
        #                 print("set a master")
        #         except Exception as e:
        #             print(e)
        #             print("fail connect to master")
        #     sleep(2)

    def set_master(self,node):
        if node.is_master:
            self.master = { "name" : node.name,"ip" : node.ip }
        else:
            raise Exception("node " + node.name + " isn't master")

    def register(self):
        if self.master:
            print(self.master.ip+ ":"+self.master.port+"/addlink")
            r = requests.post(self.master.ip+ ":"+self.master.port+"/addlink", json={self.name: self.ip})
            print(r.text)
        else:
            raise Exception("set a node master")

    def __str__(self):
        info_node = self.__dict__
        if not info_node.get("master") is None:
            print(self.master)
            nm = dict2Node(self.master)
            info_node = info_node.update({"master": {"name":nm.name,"ip":nm.ip}})
        return str(info_node)

def dict2Node(d):
    node = Node()
    for k,v in d.items():
        node.__setattr__(k,v)
    return node


class NodesController:

    def __init__(self,cluster):
        self.cluster = cluster

    def set_node_master(self,node_master):
        for node in self.cluster:
            r = requests.post(node.ip + ":"+ node.port + "/set_master", json=node_master.__dict__)
            print(r.text)


    def cluster_status(self):
        status = []
        for node in self.cluster:
            r = requests.get(node.ip + ":"+ node.port + "/info")
            status.append(r.text)
        return status

    def update_cluster(self):
        for node in self.cluster:
            r = requests.get(node.ip + ":"+ node.port + "/update")
            print(r.text)


if __name__=="__main__":

    r = requests.get("http://0.0.0.0:" + "8081" + "/info")
    d = json.loads(r.text)
    a1 = dict2Node(d)
    print(a1)
    r = requests.get("http://0.0.0.0:" + "8080" + "/info")
    d = json.loads(r.text)
    a2 = dict2Node(d)
    print(a2)
    # a1 = Node("ciccio",is_master=True)
    # a2 = Node("irene")
    # a3 = Node("ale")

    cluster = [a1,a2]

    nc = NodesController(cluster)
    #
    nc.set_node_master(a1)
    nc.update_cluster()

    # print()


    for node_status in nc.cluster_status():
        print(node_status)

