import json
from flask.wrappers import Response
from flask.globals import request
from uuid import uuid1


def tojson(f):
    def fun(*args,**kwargs):
        dump = json.dumps(f(*args,**kwargs),default=lambda o: o.__dict__)
        resp = Response(dump, status=200, mimetype='application/json')
        if "user" not in request.cookies:
            resp.set_cookie("user",str(uuid1()),secure=False)
        return resp
    fun.__name__=f.__name__
    return fun



