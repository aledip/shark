from time import sleep

from flask.app import Flask
from flask.globals import request

from src.config.AppConfig import NAME, IP
from src.models.Node import Node, dict2Node
from src.utils.WebUtils import tojson

app=Flask("shark")
node = Node(NAME,ip=IP)

while True:
    try:
        node.register()
    except:
        print("no master found")
        sleep(2)


@app.route("/info",methods=["GET"])
@tojson
def name():
    return node

@app.route("/update",methods=["GET"])
@tojson
def update():
    try:
        node.register()
        return {"message" : True}
    except:
        return {"message" : False}

@app.route("/is_master",methods=["GET"])
@tojson
def is_master():
    return {"master": node.master}

@app.route("/set_master",methods=["POST"])
@tojson
def set_master():
    j = request.get_json()
    n = dict2Node(j)
    try:
        node.set_master(n)
        return {"message": "master "+n.name+" set on node "+node.name}
    except:
        return {"message": "node "+n.name+" is not master" }

@app.route("/links",methods=["GET"])
@tojson
def links():
    return {"links": node.links}

@app.route("/addlink",methods=["POST"])
@tojson
def addlink():
    j = request.get_json()
    n = dict2Node(j)
    if str(n.ip.lower()) not in [x.ip for x in node.links]:
        node.links.append(n)
        return {"message": True}
    return {"message": False}

app.run(port=8080,host="0.0.0.0",debug=True,threaded=True)

