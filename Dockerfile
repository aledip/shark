FROM ubuntu
RUN apt-get update \
  && apt-get install -y python3-pip python3-dev \
  && cd /usr/local/bin \
  && ln -s /usr/bin/python3 python \
  && pip3 install --upgrade pip
RUN apt-get update && apt-get install -y curl
ADD . /shark
ENV PYTHONPATH=$PYTHONPATH:/shark
WORKDIR /shark/
RUN pip install --upgrade -r requirements.txt
WORKDIR /shark/src/services/
EXPOSE 9090
CMD python Service.py